package com.snake.zipperscreenlockexample

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.view.MotionEvent
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.snake.zipperscreenlockexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val IMAGE_UNZIP = intArrayOf(
        R.drawable.zz_nn_0, R.drawable.zz_nn_1, R.drawable.zz_nn_2,
        R.drawable.zz_nn_3, R.drawable.zz_nn_4, R.drawable.zz_nn_5,
        R.drawable.zz_nn_6, R.drawable.zz_nn_7, R.drawable.zz_nn_8,
        R.drawable.zz_nn_9, R.drawable.zz_nn_10, R.drawable.zz_nn_11, R.drawable.zz_nn_12
    )
    private var frameNumber = 0
    private var isDownFromStart = false
    private var mScreenHeight = 0
    private var mScreenWidth = 0
    private var mStartWidthRange = 0
    private var mEndWidthRange = 0
    private var mp: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        handleZipper()

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun handleZipper() {
        binding.zipImageView.setOnTouchListener { _, event ->
            var i = 0
            mScreenHeight = binding.zipImageView.height
            mScreenWidth = binding.zipImageView.width
            mStartWidthRange = 2 * (mScreenWidth / 5)
            mEndWidthRange = 3 * (mScreenWidth / 5)
            when (event.action) {
                MotionEvent.ACTION_DOWN -> isDownFromStart = event.y < mScreenHeight / 4 && event.x > mStartWidthRange && event.x < mEndWidthRange

                MotionEvent.ACTION_MOVE -> if (isDownFromStart) if (event.x > mStartWidthRange && event.x < mEndWidthRange
                    && isDownFromStart
                ) {
                    i = (event.y / (mScreenHeight / 13)).toInt()
                    setImage(i)
                }

                MotionEvent.ACTION_UP -> if (frameNumber > 12) {
                    frameNumber = 0
                    isDownFromStart = true
                    mp = MediaPlayer.create(
                        applicationContext,
                        R.raw.minion_sound1
                    )
                    mp?.start()
                } else {
                    frameNumber = 0
                    setImage(0)
                }

                else -> {}
            }
            true
        }
    }

    private fun setImage(paramInt: Int) {
        frameNumber = when (paramInt) {
            0 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[0])
                1
            }

            1 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[1])
                2
            }

            2 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[2])
                3
            }

            3 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[3])
                4
            }

            4 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[4])
                5
            }

            5 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[5])
                6
            }

            6 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[6])
                7
            }

            7 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[7])
                8
            }

            8 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[8])
                9
            }

            9 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[9])
                10
            }

            10 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[10])
                11
            }

            11 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[11])
                12
            }

            12 -> {
                binding.zipImageView.setImageResource(IMAGE_UNZIP[12])
                13
            }
            else -> return
        }
    }

}